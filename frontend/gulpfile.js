var gulp = require('gulp'),
    babelify = require('babelify'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),
    watchify = require('watchify');

var paths = {
    ALL: ['index.js', 'lib/*.js'],
    JS: ['main.js', 'components/*.js'],
    OUT: 'main.js',
    DEST: '../app/webroot/scripts/',
    ENTRY_POINT: './main.js'
};

gulp.task('default', ['watch']);

// Build the app
gulp.task('build', function() {

    return browserify({
        extensions: ['js'],
        entries: paths.ENTRY_POINT,
        debug: true
    })
    .transform(babelify.configure({
        ignore: /(node_modules)/
    }))
    .bundle()
    .pipe(source(paths.OUT))
    .pipe(gulp.dest(paths.DEST));
});

gulp.task('watch', function() {
    gulp.watch(paths.JS, ['build']);
});
