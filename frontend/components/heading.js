var m = require('mithril');

var Heading = {
    view: function() {
        return m("main", 
            m("p", {class: "title"}, [
                m("strong", "Stock"),
                "Portfolio"
            ])
        )
    }
}

module.exports = Heading;