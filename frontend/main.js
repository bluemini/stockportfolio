m = require('mithril');
var heading = require('./components/heading');

var boo = function()
{
    console.log('hello');
}

var count = 0;

var Hello = {
    view: function() {
        return m("main", [
            m("h1", {class: "title"}, "My first app - Hello"),
            m("button", {onclick: function() {count += 1}}, "Count: " + count),
        ])
    }
}

var elem = document.getElementById("m-binding");
if (elem) {
    m.mount(elem, heading);
} else {
    console.log('boo hoo');
}
