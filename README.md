About
=====

Some of the online portfolio tools like Yahoo! finance and Google Finance are losing functionality and I wanted a place to control my portfolio(s) without having to sacrifice functionality.

This is my attempt to build a suitable replacement.

Using Microsoft Azure and Python it will have a simple UI but delight in function.

I am building this in the open, so please feel free to contribute if you so wish.