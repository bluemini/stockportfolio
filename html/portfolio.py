import sys
import platform
import fw.pages
import re
import json


# check out : https://azure.microsoft.com/en-gb/documentation/articles/web-sites-python-configure/
# for information on web.config setup and other useful hints for pyton on azure


def hello(environ, start_response):
    return 'Hello World'


# map urls to functions
urls = [
    (r'^$', hello)
]


def application(environ, start_response):
    """
    The main WSGI application. Dispatch the current request to
    the functions from above and store the regular expression
    captures in the WSGI environment as  `myapp.url_args` so that
    the functions from above can access the url placeholders.

    If nothing matches call the `not_found` function.
    """
    resp = fw.pages.not_found()
    path = environ.get('PATH_INFO', '').lstrip('/')

    # start_response(b'200 OK', [(b'Content-Type', b'text/html')])
    # html = all_vars(environ)
    # return [html.encode('utf8')]

    try:
        for regex, callback in urls:
            match = re.search(regex, path)
            if match is not None:
                start_response(b'200 OK', [(b'Content-Type', b'text/html')])
                environ['myapp.url_args'] = match.groups()
                resp = callback(environ, start_response)
                return [resp.encode('utf8')]

    except Exception as e:
        start_response(b'500 Internal Server Error', [(b'Content-Type', b'text/html')])
        return ['<h1>Server Error</h1><p>{}</p>'.format(e).encode('utf8')]

    start_response(b'404 Not Found', [(b'Content-Type', b'text/html')])
    return ['<h1>Not Found</h1>'.encode('utf8')]
